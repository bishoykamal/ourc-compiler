try:
    import tkinter                  
except ImportError:
    print("couldn't module 'tkinter', may be you should try running 'sudo apt-get install python-tk' on your system")
    exit(0)
from tkinter import *
import tkinter.scrolledtext as ScrolledText
import tkinter.filedialog
import tkinter.messagebox
import subprocess
import os


root = tkinter.Tk(className="Lex&Yacc Compiler")
textPad = ScrolledText.ScrolledText(root, width=100, height=80)
root.geometry("500x500") #You want the size of the app to be 500x500

# create a menu & define functions for each menu item

def open_command(e = None):
        file = tkinter.filedialog.askopenfile(parent=root,mode='rb',title='Select a file')
        if file != None:
            contents = file.read()
            textPad.insert('1.0',contents)
            file.close()

def save_command(e = None):
    file = tkinter.filedialog.asksaveasfile(mode='w')
    if file != None:
    # slice off the last character from get, as an extra return is added
        data = textPad.get('1.0', END+'-1c')
        file.write(data)
        file.close()
        
def compile_command(e = None):
    directory = tkinter.filedialog.asksaveasfilename()
    if directory is not None:
        f = open("cache", "w")
        f.write(textPad.get('1.0', END+'-1c'))
        f.close()
        subprocess.call("./run_gui.sh")
        # subprocess.call("./run_gui2.sh")
        bashCommand = "./run_gui2.sh"
        process = subprocess.Popen(bashCommand, stdout=subprocess.PIPE)
        output, error = process.communicate()
        lines = output.decode("utf-8").split('\n')
        warnings = "\n".join([line for line in lines if "-warning" in line.lower()])
        errors = "\n".join([line for line in lines if "-error" in line.lower()])
        symtable = "\n".join([line for line in lines if line.startswith("--")])
        lines = "\n".join([line for line in lines if "-error" not in line.lower() and "-warning" not in line.lower() and not line.startswith("--")])
        if len(errors) > 1:
            tkinter.messagebox.showinfo("Error", errors)
            return    
        f = open(directory, "w")
        f.write(lines)
        f.close()
        os.remove("cache")
        if len(warnings) > 1:
            tkinter.messagebox.showinfo("Warning", warnings)
        if len(symtable) > 1:
            smtblWin = Toplevel(root)
            smtblWin.title("Symbol table")
            tkinter.Label(smtblWin, text=symtable,anchor='w').pack(padx=30, pady=30)


def exit_command():
    if tkinter.messagebox.askokcancel("Quit", "Do you really want to quit?"):
        root.destroy()
        

menu = Menu(root)
root.config(menu=menu)
filemenu = Menu(menu)
menu.add_cascade(label="File", menu=filemenu)
filemenu.add_command(label="Open...  ctrl+o", command=open_command)
filemenu.add_command(label="Save     ctrl+s", command=save_command)
filemenu.add_command(label="Compile  ctrl+r", command=compile_command)
filemenu.add_separator()
filemenu.add_command(label="Exit", command=exit_command)
helpmenu = Menu(menu)

root.bind('<Control-o>', open_command)
root.bind('<Control-s>', save_command)
root.bind('<Control-r>', compile_command)



textPad.pack() 
root.mainloop()

