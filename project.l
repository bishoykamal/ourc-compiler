%{
	#include <stdio.h>
    #include <iostream>
    //#include <stdlib.h>
    #include "project_helper.h" 
    #include <string>
    #include <string.h>
    #include <map>
    #include <stack>
	#include "y.tab.h" // this must be the last to include in all files
    using namespace std;
    extern bool isInScope;
    extern map<string,symbolData> symbolTable; //key is the name of variable
    extern stack<string> scopeCheck;
    extern void printSymbolTable();
    bool escape=false;
    void yyerror(std::string);
%}

%option noyywrap

%%
"//".+ { /*comments*/ }
"const" return CONST;
"do" return DO;
"while" return WHILE;
"if" return IF;
"else" return ELSE;
"print" return PRINT;
"switch" return SWITCH;
"for" {
    if(isInScope)
        scopeCheck.push("-");
    else
        isInScope=true;
    escape=true;
    return FOR;
} 
"default" return DEFAULT;
"case" return CASE;
">=" return GE;
"<=" return LE;
"==" return EQ;
"!=" return NE;
"&&" return AND;
"||" return OR;
"!" return NOT;
"#prnsymtbl" {
    printSymbolTable();
    }

(int|float|bool) {
    yylval.idName = (char*) malloc(sizeof(char) * 1024);
    strcpy (yylval.idName,yytext);
    return TYPE;
}

"true" { yylval.iValue = 1; return BOOL; }
"false" { yylval.iValue = 0; return BOOL; }

[a-zA-Z_]+[a-zA-Z0-9_]* {
    yylval.idName = (char*) malloc(sizeof(char) * 1024);
    strcpy (yylval.idName,yytext);
    return VARIABLE;
}



[0-9]+ {
    yylval.iValue = atoi(yytext);
    return INTEGER;
}

[0-9]+\.[0-9]+ { 
    yylval.dValue = atof(yytext);
    return DOUBLE;
}


[-()<>=+*/;.:] {
    return *yytext;
}

"{" {
    if(escape) 
    {
        escape=false;
        return*yytext;
    }
    if(isInScope)
        scopeCheck.push("-");
    else
        isInScope=true;
    return *yytext;
}

"}" {
    while(!scopeCheck.empty()){
        string s=scopeCheck.top();
        //cout<<"loooooo "<<s<<endl;
        if(s!="-")
        {
            if(!symbolTable[s].isUsed)
                cout<<"-Warning: "<<s<<" is not used\n";
            symbolTable.erase (s);
            scopeCheck.pop();
            //cout<<"erase "<<s<<endl;
            
        }
        else{
            scopeCheck.pop();
            break;
        }
    }
    
    if(scopeCheck.empty())
        isInScope=false;
    //cout<<"ana bara el while"<<endl;
    return *yytext;
}

[ \t]
[\n]        {yylineno++;}
. printf("-Error: unnknown character\n");

%%