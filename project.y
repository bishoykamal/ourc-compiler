
%{
    #include <iostream>
    #include <stdio.h>
    #include <stdlib.h>
    #include <stdarg.h>
    #include <map>
    #include <stack>
    #include <string>
    #include <string.h>
    #include "project_helper.h"
    using namespace std;
    //int variables[26];
    map<string,symbolData> symbolTable; //key is the name of variable
    stack<string> scopeCheck;
    bool isInScope=false;
    Node * Constant(int);
    Node * DConstant(double);
    Node * BConstant(int);
    Node * Identifier(char* i);
    Node * Operation(int operatorr, int numberOfOperands, ...);
    void insert_in_symbolTable(int,char*,char*,int);
    void multiple_declaration(char* name);
    bool is_in_symbolTable(char* name);
    bool isIntialized(char* name);
    void intialize(char* name);
    bool is_constant(char *name);
    void check_types(Node* n1,Node* n2);
    void check_unused();
    void printSymbolTable();
    int execute(Node *n);
    //int yydebug = 1;
    extern int yydebug;
    extern int yylineno;
    int yylex(void);
    void yyerror(char *s);
%}

%union {
    int iValue;
    double dValue; 
    char* idName; 
    Node *nodePtr;
};

%start program
%token <iValue> INTEGER
%token <dValue> DOUBLE
%token <iValue> BOOL
%token <idName> VARIABLE TYPE
%token WHILE IF PRINT DO ELSE SWITCH FOR DEFAULT CASE CONST MAIN END 
%left GE LE EQ NE '>' '<' NOT AND OR 
%left '+' '-'
%left '*' '/'
%type <nodePtr> program statement statements expression optional_expression cases case default

%%
program:
 statements { execute($$); check_unused(); }
 ;

statements:
 statements statement { $$ = Operation(';', 2, $1, $2); }
 | { $$ = NULL; }
 ;
   
statement:
    DO '{' statements '}' WHILE '(' expression ')' ';'			{ $$ = Operation(DO, 2, $3, $7); }
    | WHILE '(' expression ')' '{' statements '}' 				{ $$ = Operation(WHILE, 2, $3, $6);}
    | IF '(' expression ')' '{' statements '}'  				{ $$ = Operation(IF, 2, $3, $6); }
    | IF '(' expression ')' '{' statements '}' ELSE '{' statements '}'		{ $$ = Operation(IF, 3, $3, $6, $10); }
    | PRINT '(' expression ')' ';'								{ $$ = Operation(PRINT, 1, $3); }
    | SWITCH '(' expression ')' '{' cases default '}'			{ $$ = Operation(SWITCH, 3, $3, $6, $7); }
    | VARIABLE '=' expression ';'								{ 
        if(!is_in_symbolTable($1))
            {
                exit(0);
            }
        if(!is_constant($1)){
            exit(0);
        }
        string sname($1);
        Type stype= symbolTable[sname].t;
        if(stype!=$3->_type)
        {
            string s="-Error: type incompitable of "+sname;
            cout<<s<<" in line "<<yylineno<<endl;
            exit(0);
        }
        intialize($1);
        $$ = Operation('=', 2, Identifier($1), $3); /*we need to make sure that the variable is not a constant in symbol table*/}
    | CONST TYPE VARIABLE '=' expression ';'							{ multiple_declaration($3); insert_in_symbolTable(1,$2,$3,1); intialize($3); 
        string sname($3);
        Type stype= symbolTable[sname].t;
        if(stype!=$5->_type)
        {
            string s="-Error: type incompitable of "+sname;//+s1+" and "+s2;
            cout<<s<<" in line "<<yylineno<<endl;
            exit(0);
        } 
    $$ = Operation('=', 2, Identifier($3), $5); /* TODO: in the compiler, waiting for symbol table*/ }
    | FOR '(' optional_expression ';' expression ';' optional_expression ')' '{' statements '}' { $$ = Operation(FOR, 4, $3, $5, $7, $10); }
    | TYPE VARIABLE ';' {multiple_declaration($2); insert_in_symbolTable(0,$1,$2,0);  }
    | TYPE VARIABLE '=' expression ';' { multiple_declaration($2); insert_in_symbolTable(0,$1,$2,1);
        string sname($2);
        Type stype= symbolTable[sname].t;
        if(stype!=$4->_type)
        {
            string s="-Error: type incompitable of "+sname;//+s1+" and "+s2;
            cout<<s<<" in line "<<yylineno<<endl;
            exit(0);
        }
        intialize($2);
        $$=Operation('=', 2, Identifier($2), $4);  }
    ;
    
cases: 
    cases case { $$=Operation('c',2,$1,$2);}
    | { $$ = NULL; }
    ;

case: 
    CASE expression ':' statements { $$ = Operation(CASE, 2, $2, $4); }
    ;

default: 
    DEFAULT ':' statements { $$ = Operation(DEFAULT, 1, $3); }
    | { $$ = NULL; }
    ;

expression:
    INTEGER { $$ = Constant($1); }
    | DOUBLE { $$ = DConstant($1); }
    | BOOL { $$ = BConstant($1); /*TODO bishoy: edit variable declaration here if needed*/ }
    | VARIABLE {
        if(!is_in_symbolTable($1))
        {
            exit(0);
        }
        if(!isIntialized($1))
        {
            exit(0);
        }
        string sname($1);
        std::map<string,symbolData>::iterator itt;
        itt = symbolTable.find(sname);
        itt->second.isUsed=true;
        $$ = Identifier($1); /* el VARIABLE da fl grammar lma yb2a el variable 3l ymin , ba3d el equal*/ }
    | '-' expression { 
        if($2->_type!=INT||$2->_type!=FLOAT){
            string s="-Error: type incompitable  in line ";
            cout<<s<<" in line "<<yylineno<<endl;
            exit(0);
        } 
        $$ = Operation('neg', 1, $2);
        $$->_type=$2->_type;
         }
    | expression '+' expression { check_types($1,$3); $$ = Operation('+', 2, $1, $3); $$->_type=$1->_type;}
    | expression '-' expression { check_types($1,$3); $$ = Operation('-', 2, $1, $3); $$->_type=$1->_type; }
    | expression '*' expression { check_types($1,$3); $$ = Operation('*', 2, $1, $3); $$->_type=$1->_type;}
    | expression '/' expression { check_types($1,$3); $$ = Operation('/', 2, $1, $3); $$->_type=$1->_type;}
    | expression '<' expression { check_types($1,$3); $$ = Operation('<', 2, $1, $3); $$->_type=$1->_type;}
    | expression '>' expression { check_types($1,$3); $$ = Operation('>', 2, $1, $3); $$->_type=$1->_type;}
    | expression GE expression { check_types($1,$3); $$ = Operation(GE, 2, $1, $3); $$->_type=$1->_type;}
    | expression LE expression { check_types($1,$3); $$ = Operation(LE, 2, $1, $3); $$->_type=$1->_type;}
    | expression NE expression { check_types($1,$3); $$ = Operation(NE, 2, $1, $3); $$->_type=$1->_type;}
    | expression EQ expression { check_types($1,$3); $$ = Operation(EQ, 2, $1, $3); $$->_type=$1->_type;}
    | expression AND expression { 
        if($1->_type!=BOOl||$3->_type!=BOOl)
        {
            string s="-Error: type incompitable must be boolean in AND ";//+s1+" and "+s2;
            cout<<s<<" in line "<<yylineno<<endl;
            exit(0);
        }
        check_types($1,$3); $$ = Operation(AND, 2, $1, $3);$$->_type=$1->_type; }
    | expression OR expression { 
        if($1->_type!=BOOl||$3->_type!=BOOl)
        {
            string s="-Error: type incompitable must be boolean in OR";//+s1+" and "+s2;
            cout<<s<<" in line "<<yylineno<<endl;
            exit(0);
        }
        check_types($1,$3); $$ = Operation(OR, 2, $1, $3);$$->_type=$1->_type; }
    | NOT expression { 
        if($2->_type!=BOOl)
        {
            string s="-Error: type incompitable must be boolean in NOT";//+s1+" and "+s2;
            cout<<s<<" in line "<<yylineno<<endl;
            exit(0);
        }
        $$ = Operation(NOT, 1, $2);  //type check
        $$->_type=$2->_type;
     }
    | '(' expression ')' { $$ = $2; }
    ;

optional_expression:
    expression
    | VARIABLE '=' expression { 
         if(!is_in_symbolTable($1))
            {
                exit(0);
            }
        if(!is_constant($1)){
            exit(0);
        }
        string sname($1);
        Type stype= symbolTable[sname].t;
        if(stype!=$3->_type)
        {
            string s="-Error: type incompitable in "+sname;//+s1+" and "+s2;
            cout<<s<<" in line "<<yylineno<<endl;
            exit(0);
        }
        intialize($1);
        $$ = Operation('=', 2, Identifier($1), $3); /*we need to make sure that the variable is not a constant in symbol table*/}
    | TYPE VARIABLE '=' expression { multiple_declaration($2); insert_in_symbolTable(0,$1,$2,1); 
        string sname($2);
        Type stype= symbolTable[sname].t;
        if(stype!=$4->_type)
        {
            string s="-Error: type incompitable in "+sname;//+s1+" and "+s2;
            cout<<s<<" in line "<<yylineno<<endl;
            exit(0);
        }
        intialize($2); 
        $$=Operation('=', 2, Identifier($2), $4);  }
    | { $$ = NULL; }
    ;

%%

void yyerror(char *s) {
	printf("-Error: %s in line %d\n", s,yylineno);
}

int main(){
    //yydebug=1;
    yyparse();
    return 0;
}

///////////////////////////// AST CONSTRUCTORS
Node * Constant(int value){
    Node *n =  (Node *)malloc(sizeof(Node));
    n->type = CONSTANT;
    n->constant.t=INT;
    n->constant.ivalue = value;
    n->_type=INT;
    return n;
}

Node * DConstant(double value){
    Node *n =  (Node *)malloc(sizeof(Node));
    n->type = CONSTANT;
    n->constant.t=FLOAT;
    n->constant.dvalue = value;
    n->_type=FLOAT;
    return n;
}

Node * BConstant(int value)
{
    Node *n =  (Node *)malloc(sizeof(Node));
    n->type = CONSTANT;
    n->constant.t=BOOl;
    n->constant.ivalue = value;
    n->_type=BOOl;
    return n;
}


Node * Identifier(char* i){
    //printf("found %s\n",i);
    Node *n = (Node *)malloc(sizeof(Node));
    n->type = IDENTIFIER;
    n->identifier.i = (char*) malloc(sizeof(char) * 1024);
    string sname(i);
    n->_type= symbolTable[sname].t;
    strcpy (n->identifier.i,i);
    //printf("copied %s\n",n->identifier.i);
    return n;
}

Node * Operation(int operatorr, int numberOfOperands, ...){
    va_list arguments_list;
    Node *n = (Node *)malloc(sizeof(Node));
    n->operation.operands = (nodeTypeTag**)malloc(numberOfOperands * sizeof(Node));
    n->type = OPERATION;
    n->operation.operatorr = operatorr;
    n->operation.numberOfOperands = numberOfOperands;
    va_start(arguments_list, numberOfOperands);
    for (int i = 0; i < numberOfOperands; i++)
        n->operation.operands[i] = va_arg(arguments_list, Node*);
    va_end(arguments_list);
    return n;
}


template < typename T > void print( const std::stack<T>& stk )
{
    struct cheat : std::stack<T> { using std::stack<T>::c ; } ;
    const auto& seq = static_cast< const cheat& >(stk).c ;

    for( const auto& v : seq ) std::cout << v << ' ' ;
    std::cout << '\n' ;
}




void insert_in_symbolTable(int varOrconst, char* type,char* name,int intialized)
{
    string sname(name);
    string stype(type);
    symbolData temp;
    if(varOrconst==0)//0 for var 1 for const
        temp.k=KVAR;
    else if(varOrconst==1)
        temp.k=KCONST;
    if(stype=="int")
        temp.t=INT;
    else if(stype=="float")
        temp.t=FLOAT;
    else if(stype=="bool")
        temp.t=BOOl;
    if(intialized==0) //not intialized
        temp.isIntialized=false;
    else if(intialized==1)
        temp.isIntialized=true;
    temp.isUsed=false;
    symbolTable[sname]=temp;

    if(isInScope)
        scopeCheck.push(sname);
       
    // std::cout<<"Symbol Table\n";
    // for (std::map<string,symbolData>::iterator it=symbolTable.begin(); it!=symbolTable.end(); ++it)
    //     std::cout << it->first << " => " << it->second.t << '\n';
    // std::cout<<"===============\n";

    // std::cout<<"Stack\n";
    // print(scopeCheck);
    // std::cout<<"===============\n";
    //printSymbolTable();


}


void multiple_declaration(char* name)
{
    string sname(name);
    std::map<string,symbolData>::iterator itt;
    itt = symbolTable.find(sname);
    if (itt != symbolTable.end())
    {
        string s="redeclaration of "+sname;
        int n = s.length();  
        char char_array[n + 1]; 
        strcpy(char_array, s.c_str());
        yyerror(char_array);
        exit(0);

    }
}

bool is_constant(char *name){
    
    string sname(name);
    std::map<string,symbolData>::iterator itt;
    itt = symbolTable.find(sname);
    if (itt->second.k==KCONST)
    {
        string s="assignment of read-only variable "+sname;
        int n = s.length();  
        char char_array[n + 1]; 
        strcpy(char_array, s.c_str());
        yyerror(char_array);
        return false;
            
    }
    return true;

}
bool is_in_symbolTable(char* name)
{
    string sname(name);
    std::map<string,symbolData>::iterator itt;
    itt = symbolTable.find(sname);
    if (itt == symbolTable.end())
    {
        string s=sname+" was not declared";
        int n = s.length();  
        char char_array[n + 1]; 
        strcpy(char_array, s.c_str());
        yyerror(char_array);
        return false;
            
    }
    return true;

}
bool isIntialized(char* name)
{
    string sname(name);
    std::map<string,symbolData>::iterator itt;
    itt = symbolTable.find(sname);
    if(itt->second.isIntialized==false)
    {
        string s=sname+" is not intialized";
        int n = s.length();  
        char char_array[n + 1]; 
        strcpy(char_array, s.c_str());
        yyerror(char_array);
        return false;
    }
    return true;
    
}

void intialize(char* name)
{
    string sname(name);
    std::map<string,symbolData>::iterator itt;
    itt = symbolTable.find(sname);
    itt->second.isIntialized=true;
}

void check_types(Node* n1,Node* n2)
{
    if(n1->_type!=n2->_type){
            // string s1($1->identifier.i);
            // string s2($3->identifier.i);
            string s="-Error: type incompitable";//+s1+" and "+s2;
            cout<<s<<" in line "<<yylineno<<endl;
            exit(0);
        }
}

void check_unused()
{
    for (std::map<string,symbolData>::iterator it=symbolTable.begin(); it!=symbolTable.end(); ++it)
    {
        if(!it->second.isUsed)
        {
            cout<<"-Warning: "<<it->first<<" is not used\n";

        }
    }
}

void printSymbolTable()
{
    cout<<"--Symbol Table\n";
    cout<<"--=================================================\n";
    cout<<"--Symbol|Kind     |Type     |is Used  |is Intial|\n";
    cout<<"--=================================================\n";
    for (std::map<string,symbolData>::iterator it=symbolTable.begin(); it!=symbolTable.end(); ++it)
    {
        cout <<"--"<<it->first<<'\t'<<'|';
        if(it->second.k==KCONST)
            cout<<"Constant |";
        else
            cout<<"Variable |";
        if(it->second.t==INT)
            cout<<"Integer  |";
        else if(it->second.t==FLOAT)
            cout<<"Float    |";
        else if(it->second.t==BOOl)
            cout<<"Boolean  |";
        
        if(it->second.isUsed)
            cout<<"Used     |";
        else
            cout<<"Not used |";

        if(it->second.isIntialized)
            cout<<"init     |";
        else
            cout<<"Not init |";
        cout<<"\n--=================================================\n";
    }

}

        
// Interpretter
//
// int execute(Node *n)
// {
//     int expr;
//     if (!n)
//         return 0;
//     switch (n->type)
//     {
//     case CONSTANT:
//         return n->constant.value;
//     case IDENTIFIER:
//         return variables[n->identifier.i];
//     case OPERATION:
//         switch (n->operation.operatorr)
//         {
//         case WHILE:
//             while (execute(n->operation.operands[0]))
//                 execute(n->operation.operands[1]);
//             return 0;
//         case IF:
//             if (execute(n->operation.operands[0]))
//                 execute(n->operation.operands[1]);
//             else if (n->operation.numberOfOperands > 2)
//                 execute(n->operation.operands[2]);
//             return 0;
//         case FOR:
//             execute(n->operation.operands[0]);
//             while(execute(n->operation.operands[1])){
//                 execute(n->operation.operands[3]);
//                 execute(n->operation.operands[2]);
//             }
//             return 0;
//         case DO:
//             do {
//                 execute(n->operation.operands[0]);
//             } while (execute(n->operation.operands[1]));
//             return 0;
//         case SWITCH:
//             expr = execute(n->operation.operands[0]);
//             Node *cases = n->operation.operands[1];
//             while(cases!=NULL) {   
//                 if(expr==execute(cases->operation.operands[1]->operation.operands[0])) {
//                     execute(cases->operation.operands[1]->operation.operands[1]);
//                     return 0;
//                 }
//                 else
//                     cases=cases->operation.operands[0];
//             } 
//             if (n->operation.operands[2] != NULL)
//                 execute(n->operation.operands[2]->operation.operands[0]);
//             return 0;
//         case PRINT:
//             printf("%d\n", execute(n->operation.operands[0]));
//             return 0;
//         case ';':
//             execute(n->operation.operands[0]);
//             return execute(n->operation.operands[1]);
//         case '=':
//             return variables[n->operation.operands[0]->identifier.i] =
//                        execute(n->operation.operands[1]);
//         case 'neg':
//             return -execute(n->operation.operands[0]);
//         case '+':
//             return execute(n->operation.operands[0]) + execute(n->operation.operands[1]);
//         case '-':
//             return execute(n->operation.operands[0]) - execute(n->operation.operands[1]);
//         case '*':
//             return execute(n->operation.operands[0]) * execute(n->operation.operands[1]);
//         case '/':
//             return execute(n->operation.operands[0]) / execute(n->operation.operands[1]);
//         case '<':
//             return execute(n->operation.operands[0]) < execute(n->operation.operands[1]);
//         case '>':
//             return execute(n->operation.operands[0]) > execute(n->operation.operands[1]);
//         case GE:
//             return execute(n->operation.operands[0]) >= execute(n->operation.operands[1]);
//         case LE:
//             return execute(n->operation.operands[0]) <= execute(n->operation.operands[1]);
//         case NE:
//             return execute(n->operation.operands[0]) != execute(n->operation.operands[1]);
//         case EQ:
//             return execute(n->operation.operands[0]) == execute(n->operation.operands[1]);
//         case AND:
//             return execute(n->operation.operands[0]) && execute(n->operation.operands[1]);
//         case OR:
//             return execute(n->operation.operands[0]) || execute(n->operation.operands[1]);
//         case NOT:
//             return !execute(n->operation.operands[0]);
//         }
//     }
//     return 0;
// }


// COMPILER
//
static int lbl;
int execute(Node *n) {
    int lbl1, lbl2;
    if (!n)
        return 0;
    switch (n->type)
    {
    case CONSTANT:
        if(n->constant.t==INT)
            printf("\tpush\t%d\n", n->constant.ivalue);
        if(n->constant.t==FLOAT)
            printf("\tpush\t%f\n", n->constant.dvalue);
        break;
    case IDENTIFIER:
        printf("\tpush\t%s\n", n->identifier.i); 
        break;
    case OPERATION:
        switch (n->operation.operatorr)
        {
        case WHILE:
            printf("L%03d:\n", lbl1 = lbl++);
            execute(n->operation.operands[0]);
            printf("\tjz\tL%03d\n", lbl2 = lbl++);
            execute(n->operation.operands[1]);
            printf("\tjmp\tL%03d\n", lbl1);
            printf("L%03d:\n", lbl2);
            break;
        case DO:
            printf("L%03d:\n", lbl1 = lbl++);
            execute(n->operation.operands[0]);
            execute(n->operation.operands[1]);
            printf("\tjnz\tL%03d\n", lbl1);
            break;
        case FOR:
            execute(n->operation.operands[0]);
            printf("L%03d:\n", lbl1 = lbl++);
            execute(n->operation.operands[1]);
            printf("\tjz\tL%03d\n", lbl2 = lbl++);
            execute(n->operation.operands[2]);
            execute(n->operation.operands[3]);
            printf("\tjmp\tL%03d\n", lbl1);
            printf("L%03d:\n", lbl2);
            break;
        case IF:
            execute(n->operation.operands[0]);
            if (n->operation.numberOfOperands > 2)
            {
                /* if else */
                printf("\tjz\tL%03d\n", lbl1 = lbl++);
                execute(n->operation.operands[1]);
                printf("\tjmp\tL%03d\n", lbl2 = lbl++);
                printf("L%03d:\n", lbl1);
                execute(n->operation.operands[2]);
                printf("L%03d:\n", lbl2);
            }
            else
            {
                /* if */
                printf("\tjz\tL%03d\n", lbl1 = lbl++);
                execute(n->operation.operands[1]);
                printf("L%03d:\n", lbl1);
            }
            break;
        case SWITCH:
        {
            Node *cases = n->operation.operands[1];
            lbl2=lbl++;
            while (cases != NULL){
                execute(n->operation.operands[0]);
                execute(cases->operation.operands[1]->operation.operands[0]);
                printf("\tcompEQ\n");
                printf("\tjz\tL%03d\n", lbl1 = lbl++);
                execute(cases->operation.operands[1]->operation.operands[1]);
                printf("\tjmp\tT%03d\n", lbl2);
                printf("L%03d:\n", lbl1);
                cases = cases->operation.operands[0];
            }
            if (n->operation.operands[2] != NULL)
                execute(n->operation.operands[2]->operation.operands[0]);
            printf("T%03d:\n", lbl2);
            }

            break;
        case PRINT:
            execute(n->operation.operands[0]);
            printf("\tprint\n");
            break;
        case '=':
            execute(n->operation.operands[1]);
            printf("\tpop\t%s\n", n->operation.operands[0]->identifier.i); //this is how it converts index to character
            break;
        case 'neg':
            execute(n->operation.operands[0]);
            printf("\tneg\n");
            break;
        default:
            execute(n->operation.operands[0]);
            execute(n->operation.operands[1]);
            switch (n->operation.operatorr)
            {
            case '+':
                printf("\tadd\n");
                break;
            case '-':
                printf("\tsub\n");
                break;
            case '*':
                printf("\tmul\n");
                break;
            case '/':
                printf("\tdiv\n");
                break;
            case '<':
                printf("\tcompLT\n");
                break;
            case '>':
                printf("\tcompGT\n");
                break;
            case GE:
                printf("\tcompGE\n");
                break;
            case LE:
                printf("\tcompLE\n");
                break;
            case NE:
                printf("\tcompNE\n");
                break;
            case EQ:
                printf("\tcompEQ\n");
                break;
            case AND:
                printf("\tand\n");
                break;
            case OR:
                printf("\tor\n");
                break;
            case NOT:
                printf("\tnot\n");
                break;
            }
        }
    }
    return 0;
}