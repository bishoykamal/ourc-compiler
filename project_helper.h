
typedef enum{
    KVAR,
    KCONST
    
}kind; //constant or variabl

typedef enum{
    INT,
    FLOAT,
    BOOl
}Type; //data type. hnbd2 b dol w hnzwd tany



typedef enum
{
    CONSTANT,
    IDENTIFIER,
    OPERATION
} nodeType;

/* constants */
typedef struct
{
    Type t;
    union 
    {
        int ivalue; /* value of constant */
        double dvalue;
    };

} constantNode;

/* identifiers */
typedef struct
{
    char* i; /* subscript to sym array */
} identifierNode;

/* operators */
typedef struct
{
    int operatorr;                /* operator */
    int numberOfOperands;                /* number of operands */
    struct nodeTypeTag **operands; /* operands */ //hena est5dmna esm el struct nafso 3ashan yt3ml fl compile type 3ashan hwa asln mt3rraf ta7to (circular dependency)
} operationNode;

typedef struct nodeTypeTag
{
    Type _type;
    nodeType type; /* type of node */
    union {
        constantNode constant; /* constants */
        identifierNode identifier;   /* identifiers */
        operationNode operation; /* operators */
    };
} Node;

typedef struct 
{
    kind k; //var or const
    Type t; //int or float
    bool isIntialized;
    bool isUsed;
    
    
}symbolData;

//extern int symbolTable[26];
