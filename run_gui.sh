#!/bin/sh
rm lex.yy.c y.tab.c y.tab.h
lex project.l 
yacc -d --debug --verbose project.y
#gcc -g -w lex.yy.c y.tab.c 
g++ -w y.tab.c lex.yy.c --std=c++14
